
import hashlib
from datetime import datetime
from json import JSONDecodeError

import os

import requests

from parent_parser import Parser
from row_objects import CostRow, RevenueRow


class PlugrushParser(Parser):

    required_auth_fields = ('username', 'api_key')

    def _generate_timestamp(self):
        """
        Не удалось быстро разобраться в таймзонах, поэтому таймстемп
        получаем вот таким костылем
        """
        return str(int((datetime.utcnow()-datetime(1970,1,1)).total_seconds()))

    def _generate_hash(self, access_token, timestamp, email):
        m = hashlib.sha256()
        m.update((access_token + '|' + timestamp + '|' + email).encode('utf8'))
        return m.hexdigest()

    def _make_request(self, **kwargs):
        """
        Сделать запрос к плагу, проверить на валидность, вернуть результат
        """
        timestamp = self._generate_timestamp()
        access_token = self.auth['api_key']
        email = self.auth['username']
        kwargs['params'].update({
            'timestamp': timestamp,
            'email': email,
            'hash': self._generate_hash(access_token, timestamp, email)
        })
        res = requests.request(**kwargs)
        try:
            res = res.json()
        except JSONDecodeError:
            if 'Too Many Attempts'.lower() in res.text.lower():
                self._raise_error('Too many attempts')
            if '504 Gateway Time-out'.lower() in res.text.lower():
                self._raise_error(f'504 Gateway Time-out for {kwargs}',
                                  trace=False)

            self._raise_error(f'Error for {kwargs}: {res.text}')

        message_text = res['message'].lower()
        if 'success' not in message_text and 'no statistics' not in message_text:
            self._raise_error('Error connecting to Plugrush API. {}'.format(res))

        # если статистики нет, то data будет равен null,
        # и итерация вызовет ошибку. Предупреждаем это
        return res['data'] or []

    def revenue_countries(self, date, zone_id):
        data = self._make_request(
            url='http://admin.plugrush.com/api/v2/stats/publisher/countries',
            params={
                'fromDate': date.strftime('%Y-%m-%d'),
                'toDate': date.strftime('%Y-%m-%d'),
                'widgets': zone_id
            },
            method='GET'
        )
        rows = []

        for row in data:
            if not int(row['uniques']):
                continue
            visits = int(row['uniques'])
            country_code = row['cc']
            revenue = float(row['income'])
            rows.append(
                RevenueRow(
                    name=country_code,
                    date=date,
                    visits=visits,
                    revenue=revenue
                )
            )
        return rows


if __name__ == '__main__':

    parser = PlugrushParser(
        auth={
            'api_key': os.environ.get('PLUGRUSH_API_KEY'),
            'username': os.environ.get('PLUGRUSH_USERNAME'),
        }
    )
    data = parser.revenue_countries(
        date=datetime(2017,11,8).date(), zone_id=1143245
    )
    for row in data:
        print(row)

    """
    этот вызов возвращает:
    
    name:Unknown,date:2017-11-08,visits:84,revenue:0.0
    name:AL,date:2017-11-08,visits:397,revenue:0.013352
    name:EG,date:2017-11-08,visits:1,revenue:0.0
    name:IE,date:2017-11-08,visits:7,revenue:0.004538
    name:DE,date:2017-11-08,visits:3,revenue:0.004107
    name:HU,date:2017-11-08,visits:24,revenue:0.003375
    name:IL,date:2017-11-08,visits:504,revenue:0.165685
    name:PT,date:2017-11-08,visits:2,revenue:0.000375
    name:QA,date:2017-11-08,visits:116,revenue:0.112401
    name:RS,date:2017-11-08,visits:82,revenue:0.00153
    name:ZA,date:2017-11-08,visits:5,revenue:0.002432
    name:SN,date:2017-11-08,visits:218,revenue:0.023127
    name:UA,date:2017-11-08,visits:1,revenue:0.000188
    """
